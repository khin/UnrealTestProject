// Fill out your copyright notice in the Description page of Project Settings.

#include "MyLittleProject.h"
#include "FloatingActor.h"


// Sets default values
AFloatingActor::AFloatingActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	height = 20.0f;
}

// Called when the game starts or when spawned
void AFloatingActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFloatingActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	Move(DeltaTime);
}

void AFloatingActor::Move(float deltaTime) {
	FVector newPosition = GetActorLocation();
	float deltaHeight = FMath::Sin(runningTime + deltaTime) - FMath::Sin(runningTime);

	newPosition.Z += deltaHeight * height;
	runningTime += deltaTime;

	SetActorLocation(newPosition);
}

